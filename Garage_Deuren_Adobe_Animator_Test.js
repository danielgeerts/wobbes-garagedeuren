(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 720,
	height: 480,
	fps: 24,
	color: "#FFFFFF",
	manifest: []
};



// symbols:



(lib.bluegarage = function() {
	this.spriteSheet = ss["Garage_Deuren_Adobe_Animator_Test_atlas_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.graygarage = function() {
	this.spriteSheet = ss["Garage_Deuren_Adobe_Animator_Test_atlas_"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.whitegarage = function() {
	this.spriteSheet = ss["Garage_Deuren_Adobe_Animator_Test_atlas_"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.sButton = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Button_1
	this.txt = new cjs.Text("custom txt", "8px 'Arial'");
	this.txt.name = "txt";
	this.txt.textAlign = "center";
	this.txt.lineHeight = 9;
	this.txt.lineWidth = 86;
	this.txt.setTransform(236.5,20,5.5,7.659);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFE4CC").s().p("EgmlAH0IAAvoMBNLAAAIAAPog");
	this.shape.setTransform(247.1,50.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.txt}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,495,120.3);


(lib.background_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.whitegarage();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,720,480);


(lib.background_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.graygarage();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,720,480);


(lib.background_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.bluegarage();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,720,480);


(lib.Scene1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{frame_1:0});

	// Actions
	this.btn = new lib.sButton();
	this.btn.setTransform(0,0,0.429,0.429);
	new cjs.ButtonHelper(this.btn, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get(this.btn).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,212.3,51.6);


// stage content:
(lib.Garage_Deuren_Adobe_Animator_Test = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
		this.custom_1.btn.txt.text = "Verander Achtergrond";
		this.custom_1.addEventListener("click", changeBackground.bind(this));
		
		function changeBackground() {
			if (this.background_1.visible) {
				//this.setBackground(false, true, false);
				this.background_1.visible = false;
				this.background_2.visible = true;
				this.background_3.visible = false;
			} else if (this.background_2.visible) {
				//this.setBackground(false, false, true);
				this.background_1.visible = false;
				this.background_2.visible = false;
				this.background_3.visible = true;
			} else if (this.background_3.visible) {
				//this.setBackground(true, false, false);
				this.background_1.visible = true;
				this.background_2.visible = false;
				this.background_3.visible = false;
			}
		}
		/*
		function setBackground(a:boolean, b:boolean, c:boolean):void {
			this.background_1.visible = a;
			this.background_2.visible = b;
			this.background_3.visible = c;
		}
		*/
		var counter = 0;
		
		this.custom_2.btn.txt.text = "Click counter " + counter;
		this.custom_2.addEventListener("click", changeColor.bind(this));
		
		function changeColor() {
			counter++;
			this.custom_2.btn.txt.text = "Click counter " + counter;
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Button_1
	this.custom_2 = new lib.Scene1();
	this.custom_2.setTransform(20,71.6);

	this.timeline.addTween(cjs.Tween.get(this.custom_2).wait(1));

	// Button_2
	this.custom_1 = new lib.Scene1();
	this.custom_1.setTransform(20,20);

	this.timeline.addTween(cjs.Tween.get(this.custom_1).wait(1));

	// Background_1
	this.background_1 = new lib.background_1();
	this.background_1.setTransform(360,240,1,1,0,0,0,360,240);

	this.timeline.addTween(cjs.Tween.get(this.background_1).wait(1));

	// Background_2
	this.background_2 = new lib.background_2();
	this.background_2.setTransform(0,0,1.2,1.2);

	this.timeline.addTween(cjs.Tween.get(this.background_2).wait(1));

	// Background_3
	this.background_3 = new lib.background_3();
	this.background_3.setTransform(511.5,383.6,1,1,0,0,0,511.5,383.6);

	this.timeline.addTween(cjs.Tween.get(this.background_3).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(360,240,864,576);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;